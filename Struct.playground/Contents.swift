//: Playground - noun: a place where people can play

import UIKit

typealias BoardPiece = String
let X: BoardPiece = "X"
let O: BoardPiece = "O"



//Battle-Ship game
/*
 Rule:
    - Fill the head point and of Ship, and the we create a Ship for you
    - The player choice one point in coordinates (0,0)...(10,10)
    - Play until your point hit head ship
    - Hope you have fun with this game !!! Thanks for playing

*/
struct point{
    var x : Int?
    var y : Int?
}



struct ship{
    var point : point
    let directionOfShip : String
    func bodyOfShip() -> [(x: Int, y: Int)?]?{
        var ship = [(Int,Int)?]()
        
        if let a = point.x, let b = point.y {
            switch directionOfShip {
            case "North":
                if a <= 9 && a >= 1 && b >= 4 && b <= 10{
                    ship += [(a,b),(a,b-1),(a-1,b-1),(a+1,b-1),(a,b-2),(a,b-3),(a-1,b-3),(a+1,b-3)]
                }else{
                    print("Please fill new avaiable value")
                    return nil
                }
                break
            case "South":
                if a <= 9 && a >= 1 && b >= 0 && b <= 7{
                    ship += [(a,b),(a,b+1),(a-1,b+1),(a+1,b+1),(a,b+2),(a,b+3),(a-1,b+3),(a+1,b+3)]
                }else{
                    print("Please fill new avaiable value")
                    return nil
                }
                break
            case "East":
                if a <= 10 && a >= 4 && b >= 1 && b <= 9{
                    ship += [(a,b),(a-1,b),(a-1,b-1),(a-1,b+1),(a-2,b),(a-3,b),(a-3,b-1),(a-3,b+1)]
                }else{
                    print("Please fill new avaiable value")
                    return nil
                }
                break
            case "West":
                if a <= 7 && a >= 0 && b >= 1 && b <= 9{
                    ship += [(a,b),(a+1,b),(a+1,b-1),(a+1,b+1),(a+2,b),(a+3,b),(a+3,b-1),(a+3,b+1)]
                }else{
                    print("Please fill new avaiable value")
                    return nil
                }
                break
            default:
                print("Please fill new avaiable value")
                return nil
                break
            }
        }
        return ship
    }
    
    mutating func shotShip(x : Int?,y: Int?) -> Bool {
        var a = true
        guard var shotShip = bodyOfShip() else {
            print("Please create a Ship to play!!!")
            return false
        }
        for (index,value) in shotShip.enumerated(){
            if let value = value {
                if value.0 == x && value.1 == y{
                    if index == 0 {
                        print("Headshot. This is destroyed!!! Game Over")
                        return true
                    }else{
                        print("Ship is is shot by (\(x!),\(y!))")
                        return true
                    }
                }else {
                    a = false
                }
            }else{
                print("Can't shop this place again")
                return false
            }
        }
        if a != true{
            print("Failed")
            return false
        }
        return a
    }
}
var a : point = point(x: 5,y: 7)
var Ship = ship(point: a, directionOfShip: "North")
//Ship.bodyOfShip()
Ship.shotShip(x: 6, y: 5 )
/*
    I have a error
    i want if i shot point, and shot it again, print "You shot here once time !!"
    please tell how to do that??
 */


