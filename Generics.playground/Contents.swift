//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

// generics func
func add<Element>(Array: inout[Element], newElement: Element) {
    Array.append(newElement)
}

var arr = [Int]()
add(Array: &arr, newElement: 3)

var string = [String]()
add(Array: &string, newElement: "hello")

// generics Class

class Movies<T: Equatable> {
    var list: [T] = []
    func listMovie(new: T) {
        return list.append(new)
    }
    func removeMovie(Movie: T) -> T? {
        return !list.isEmpty && list.contains(Movie) ? list.remove(at: list.index(of: Movie)!) : nil
    }
}

//class Movie: Equatable {
//    var name: String
//    var direction: String
//    init(name: String, direction: String) {
//        self.name = name
//        self.direction = direction
//    }
//}


var movies = Movies<String>()
movies.listMovie(new: "Star War")
movies.listMovie(new: "Avengers")
movies.listMovie(new: "X-men")
print(movies.list)
movies.removeMovie(Movie: "X-men")
print(movies.list)

typealias movie = [String: String]
var starWar = ["StarWar": "Abdbsabdab"]
var Xmen = ["X-men": "Sony"]
var list = Movies<movie>()
list.listMovie(new: starWar)
list.listMovie(new: Xmen)
list.removeMovie(Movie: starWar)
print(list.list)

protocol Media {
    var name: String {get set}
}

class Movie: Media, CustomStringConvertible {

    var name: String
    var direction: String
    init(name: String, direction: String) {
        self.name = name
        self.direction = direction
    }
    var description: String {
        return "Movie: \(name) dir.\(direction)"
    }
}

class Song: Media, CustomStringConvertible {
    var name: String
    var singer: String
    init(name: String, singer: String) {
        self.name = name
        self.singer = singer
    }
    var description: String {
        return "Song: \(name), by \(singer)"
    }
}


var library: [Media] = [
    Movie(name: "Star War", direction: "ABC"),
    Song(name: "aaaaa", singer: "bbbbb"),
    Song(name: "ccccc", singer: "ccccc"),
    Movie(name: "X-men", direction: "20th"),
    Movie(name: "Avergers", direction: "Marvel"),
]

library.forEach{
    if let movie = $0 as? Movie {
        print("Movie: \(movie.name), dir. \(movie.direction)")
    }else if let song = $0 as? Song {
        print("Song: \(song.name), by \(song.singer)")
    }
}

print(library.filter {$0 is Movie})

//
func swap<T>(a: inout T, b: inout T){
    let temp = a
    a = b
    b = temp
    print("a: \(a), b: \(b)")
}
var a = "str1"
var b = "str2"

//
enum Optional<T> {
    case none
    case some(T)
}


protocol Animal: CustomStringConvertible {
    var name: String {get set}
}

class Cat: Animal {
    var description: String {
        return "Cat: \(name)"
    }
    
    var name: String
    init(name: String) {
        self.name = name
    }
}
class Dog: Animal {
    var description: String {
        return "Dog: \(name)"
    }
    
    var name: String
    init(name: String) {
        self.name = name
    }
}
class Keeper<Animal> {
    var name: String
    var lookAfter: [Animal] = []
    var list: [[Animal]] = []
    subscript(index: Int) -> Animal {
        get {
            assert(lookAfter[index] != nil , "Don't have value")
            return lookAfter[index]
        }
        set(newValue) {
            assert(lookAfter[index] != nil , "Don't have value")
            lookAfter[index] = newValue
        }
    }
    subscript<T: Cat>(animal: T) -> String {
        return "🐱: \(animal.name)"
    }
    subscript(row: Int, column: Int) -> Animal {
        get {
            assert(list[row][column] != nil , "Don't have value")
            return list[row][column]
        }
        set {
            assert(list[row][column] != nil , "Don't have value")
            list[row][column] = newValue
        }
    }
    init(name: String) {
        self.name = name
    }
}

var catA = Cat(name: "A")
var dogA = Dog(name: "AA")
var john = Keeper<Animal>(name: "John")
john.lookAfter.append(catA)
john[0] = dogA
john.list.append([catA,dogA])
print(john[0])
print(john[0, 1])
//var aaaa = Keeper<Int>(name: "Abc")


protocol A {
    associatedtype T
    var rawValue: String {get}
}

//extension A {
//    var key: String {
//        return rawValue
//    }
//    var hashValue : Int {
//        return key.hashValue
//    }
//}


enum B<T>: String, A {
    case a = "abc"
    case b = "bac"
    case c = "cab"
    var Value: T {
        return Value
    }
}

var n = [1,2,3]

extension Keeper where Animal: Cat {
    func printCat() {
        print(self.lookAfter)
    }
}
var ana = Keeper<Cat>(name: "Ana")
ana.lookAfter.append(catA)
ana.printCat()
print(john[catA])










