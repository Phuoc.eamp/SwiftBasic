//: Playground - noun: a place where people can play

import UIKit
//bai 1
var sum = 0
for i in 0...5 {
    sum += i
}
print(sum)
//bai 2
var aLotOfAs = ""
while aLotOfAs.characters.count < 10 {
    aLotOfAs += "a"
}
print(aLotOfAs)
//bai 3
var coordinates = (2,2,2)
switch coordinates {
case let (x, y, z) where x == y && y == z:
    print("x = y = z")
case (_, _, 0):
    print("On the x/y plane")
case (_, 0, _):
    print("On the x/z plane")
case (0, _, _):
    print("On the y/z plane")
default:
    print("Nothing special")
}
 // bai 5
var i : Int = 10
repeat{
    print("countdown: \(i)")
    i -= 1
}while i >= 0
 // bai 6
var a = 0.0
for _ in 0...10{
    print(a)
    a += 0.1
}
//repeat{
//    print(a)
//    a += 0.1
//}while a <= 1.0
//print(a)

