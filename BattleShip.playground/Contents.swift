//: Playground - noun: a place where people can play

import UIKit

var mess = """
    BattleShip Game

    Rule:
     - Fill the head point and of Ship, and the we create a Ship for you
     - The player choice one point in coordinates (0,0)...(10,10)
     - Play until your point hit head ship
     - Hope you have fun with this game !!! Thanks for playing
 """

struct Point{
    var x: Int
    var y: Int
    init(x: Int,y: Int) {
        self.x = x
        self.y = y
    }
    init() {
        self.x = 0
        self.y = 0
    }
    
    func compare(point: Point) -> Bool{
        guard self.x == point.x && self.y == point.y else{
            return false
        }
        return true
    }
    
    mutating func random() {
        x = Int(arc4random_uniform(10))
        y = Int(arc4random_uniform(10))
        
    }
}

struct Ship{
    let direction: String
    var ship: [Point]
    var point: [Point] = []
    
    init(direction: String) {
        self.direction = direction
        self.ship = []
        print("Play Game!!!")
        print("Create a ship")
        print("Play again")
        print("Classic")
        print("Random")
        print("Exit")
    }
    // func play
    mutating func play(option: String){
        switch option {
        case "random":
            print("Please fill a count")
            break
        case "classic":
            print("Please fill your shot")
            break
        case "exit,play again":
            clean()
            break
        default:
            print("Please fill your choice")
        }
    }
    func check() -> Bool{
        if ship.isEmpty {
            return true
        }
        return false
    }
    // func create a ship
    mutating func bodyOfShip() -> [Point]?{
        if check(){
        var headShip = Point()
        headShip.random()
        var a : Point = Point()
        var b : Point = Point()
        var c : Point = Point()
        var d : Point = Point()
        var e : Point = Point()
        var f : Point = Point()
        var g : Point = Point()
        
            switch direction {
            case "North":
                guard headShip.x <= 9 && headShip.x >= 1 && headShip.y >= 4 && headShip.y <= 10 else{
                    bodyOfShip()
                    return nil
                }
                a = Point(x: headShip.x, y: headShip.y - 1)
                b = Point(x: headShip.x - 1, y: headShip.y - 1)
                c = Point(x: headShip.x + 1, y: headShip.y - 1)
                d = Point(x: headShip.x, y: headShip.y - 2)
                e = Point(x: headShip.x, y: headShip.y - 3)
                f = Point(x: headShip.x - 1, y: headShip.y - 3)
                g = Point(x: headShip.x + 1, y: headShip.y - 3)
                ship = [headShip,a,b,c,d,e,f,g]
                //print(headShip)
                //print(ship)
                return ship
            
                
            default:
                print("Please fill direction of Ship")
                break
            }
        }
        
        return nil
    }
    
    // func shot random
    mutating func shotRandom(count: inout Int) -> Int{
        if check() == false{
            while count > 0{
                let shot = Point(x: Int(arc4random_uniform(10)), y: Int(arc4random_uniform(10)))
                if point.contains(where: {$0.compare(point: shot)}) == false{
                    point += [shot]
                    if ship.first!.compare(point: shot){
                        print("Headshot. This is destroyed!!! Game Over")
                        clean()
                        return 0
                    }else if ship.contains(where: {$0.compare(point: shot)}) {
                        print("Ship is is shot by (\(shot.x),\(shot.y))")
                    }else {
                        print("(\(shot.x),\(shot.y)) is Failed!!!")
                    }
                    count -= 1
                }
                
            }
        }
        return 0
    }
    
    //func shot classical
    mutating func shot(yourpoint: [Point]){
        yourpoint.compactMap {$0}
        if !check(){
            for points in yourpoint{
                if point.contains(where: {$0.compare(point: points)}) == true{
                    print("You shotted at this point!!!")
                }else {
                    point += [points]
                    if ship.first!.compare(point: points){
                        print("Headshot. This is destroyed!!! Game Over")
                        clean()
                        continue
                    }else if ship.contains(where: {$0.compare(point: points)}) {
                        print("Ship is is shot by (\(points.x),\(points.y))")
                    }else {
                        print("(\(points.x),\(points.y)) is Failed!!!")
                    }
                }
            }
        }
    }
    
    mutating func clean(){
        ship = []
        point = []
    }
    
}
var a = Point(x: 0, y: 4)
var b = Point(x: 9, y: 9)
var c = Point(x: 2, y: 5)
var d = Point(x: 6, y: 1)
var e = Point(x: 1, y: 6)
var f = Point(x: 0, y: 4)
var arr = [a,b,c,d,e]
arr.contains{$0.compare(point: f)}



var ship = Ship(direction: "North")






// demo
struct abc{
    var b: Int
    init() {
        let a = Int(arc4random_uniform(10))
        b = a
    }
    mutating func increment() -> Int{
        b += 1
        //print(b)
        return b
    }
}
var aa = abc()
aa.b
aa.increment()
aa.increment()













