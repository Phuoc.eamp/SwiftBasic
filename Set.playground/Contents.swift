//: Playground - noun: a place where people can play

import UIKit


var number1: Set = [1,2,3,4,5,6,7]
var number2: Set = [3,4,8,9,10,11]
var number3: Set = [1,2,3]
var number4: Set = [0]
var number5: Set = [1,2,3,4,5,6,7]
print(number1.intersection(number2))
print(number1.union(number2).sorted())
print(number1.subtracting(number2).sorted())
print(number1.symmetricDifference(number2))
number1.isSuperset(of: number3)
number3.isSubset(of: number1)
number1.isDisjoint(with: number4)
number1.isStrictSubset(of: number5)
number1 == number2
number1 == number5
print(number1.filter {$0 < 3})
var number6: Set = [1,2,3,4,3,2,6,5,6]
number6.remove(1)
number6.removeFirst()
print(number6)
var str1: Set = ["a","b","c"]
var dict: [Int: Int] = [:]
number3.map{dict[$0] = $0}
print(dict)
var str = ["h","e","l","l","o"]
//var mess: String = nil
var mess = str.reduce("") {$0 + $1}
print(mess)
"h" + "e"
var primes: Set = [2, 3, 5, 7]
let primeStrings = primes.sorted().map(String.init)


print(str1.map {$0.uppercased()})

class Point {
    var x: Int
    var y: Int
    init(x: Int,y: Int) {
        self.x = x
        self.y = y
    }
}
extension Point: Hashable {
    var hashValue: Int {
        return x.hashValue + y.hashValue
    }
    
    static func == (lhs: Point, rhs: Point) -> Bool {
        return lhs.x == rhs.x && lhs.y == rhs.y
    }
    
}

var set: Set<Point> = [Point(x: 0, y: 1), Point(x: 1, y: 0)]
let point = Point(x: 1, y: 1)
if set.contains(point) {
    print("Point at (\(point.x), \(point.y))")
}else {
    set.insert(point)
    print("New point: (\(point.x), \(point.y))")
}
let point1 = Point(x: 1, y: 1)
point.hashValue == point1.hashValue
point.x.hashValue == point1.x.hashValue

class person: Hashable {
    var hashValue: Int {
        return email.hashValue
    }
    var name: String
    var age: Int
    var email: String
    init(name: String, age: Int, email: String) {
        self.name = name
        self.age = age
        self.email = email
    }
    static func == (lhs: person, rhs: person) -> Bool {
        return lhs.age == rhs.age && lhs.email == rhs.email && lhs.name == rhs.name
    }
}

var arr = [1,2,3]





