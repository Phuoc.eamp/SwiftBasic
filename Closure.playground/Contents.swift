//: Playground - noun: a place where people can play

import UIKit
// print "Hello!" times.
func repeatTask(times: Int, task: () -> Void){
    for _ in 0..<times{
        task()
    }
}
var task: () -> Void = {
    print("Hello !")
}
repeatTask(times: 3, task: task)
// caculate average point
let appRatings = [
    "Calendar Pro": [1, 5, 5, 4, 2, 1, 5, 4],
    "The Messenger": [5, 4, 2, 5, 4, 1, 1, 2],
    "Socialise": [2, 1, 2, 2, 1, 2, 4, 2]
]
//print(appRatings.values)

var averageRatings = [String: Double]()
for(key,value) in appRatings{
   
    let a = value.reduce(0){$0+$1}
    averageRatings[key] = Double(a)/Double(value.count)
}
print(averageRatings)

// show Student have point greater than 3
var Student = averageRatings.filter {$0.value >= 3}
print(Student )


