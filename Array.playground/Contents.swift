//: Playground - noun: a place where people can play

import UIKit
import Foundation

// remove first element has value equals item
func removingOnce(_ item: Int, from array: inout[Int]) -> [Int]{
    for (index, value) in array.enumerated() {
        if value == item{
            array.remove(at: index)
            return array
        }
    }
    return array
}
var arr = [1,2,1,3,1,5]
print(removingOnce(1, from: &arr))

//remove element have value equals item
func removing(_ item: Int, from array: inout[Int]) -> [Int]{
    let arrA = array.filter {$0 != item}
    array = arrA
    
    return array
}
removing(1, from: &arr)
print(arr)

//reversed
func reversed(_ array: inout[Int]) -> [Int]{
    var arr = array

    for i in 0..<array.count {
        array[i] = arr[array.count - 1 - i]
    }
    return array
}
var arrA = [1,2,3,4,5,6]
reversed(&arrA)

// min and max of array
func minMax(of numbers: [Int]) -> (min: Int, max: Int)?{
    var min = numbers[0]
    var max = min
    for i in 1..<numbers.count{
        if min > numbers[i]{
            min = numbers[i]
        }
        if max < numbers[i]{
            max = numbers[i]
        }
        print(min,max)
    }
    return (min,max)
}
var arrrr = [0,5,2,10,1,20]
minMax(of: arrrr)


