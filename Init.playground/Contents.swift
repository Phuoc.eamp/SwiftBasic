//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

class Person{
    var name: String
    var age: Int
    init(name: String,age: Int) {
        self.name = name
        self.age = age
    }
    deinit {
        print("No student")
    }
}
class Student: Person {
    var school: String
    
    init(school: String,name: String,age: Int) {
        self.school = school
        super.init(name: name, age: age)
    }
    override init(name: String, age: Int) {
        self.school = "Oxford"
        super.init(name: name, age: age)
    }
    
}

extension Person{
    var fullname: String {
        get{
            return self.name
        }
        set{
            self.name = newValue
        }
    }
    convenience init?(yearOfBirth: Int, name: String){
        guard yearOfBirth <= 2018 else {
            return nil
        }
        self.init(name: name, age: 2018 - yearOfBirth)
    }
}

var john : Person? = Person(name: "John", age: 18)
//if let john = john{
//    john.age
//    print(john.fullname)
//    john.fullname = "Alex John"
//    john.name
//}



//john = Person(yearOfBirth: 2000, name: "alex")
//john?.age
////john = nil
var army : Student? = Student(school: "oxford", name: "Amry", age: 19)
army!.fullname
print(john!.age)
func inforOfPerson(person: Person){
    print("""
            Name: \(person.fullname)
            Age : \(person.age)
            """)
}
inforOfPerson(person: army!)
//army = nil

//
class A{
    var a: Int?
    
//    init() {
//    }
    
    init(a: Int?) {
        self.a = a
        print("A = \(a)")
    }
}
class B: A{
    var b: Int?
    
    init(b: Int?, a: Int?){
        super.init(a: a)
        self.b = b
        print("B = \(b)")
    }
    
    func sum() -> Int{
        return a! + b!
    }
}
class C: B{
//    var c: Int
//
//    init(c: Int) {
//        self.c = c
//        super.init(b: b, a: a)
//        print("C = \(c)")
//    }
//
//
//    override func sum() -> Int {
//        return super.a + super.b + self.c
//    }
}
//class D: C{
//    override var c: Int{
//        get{
//            return super.c
//        }
//        set{
//            super.a = newValue
//        }
//
//    }
//
//
//}
//let a = A(a: 6)
let b = B(b: 5, a: 6)
b.sum()


//let c = C(c: 5)
//c.sum()




//var d = D(c: 7)
//d.c = 4
//d.a

//
//class classA{
//    var a: Int
//    required init(){
//        var a = 10
//        print(a)
//    }
//
//}
//class classB: classA {
//    required init() {
//        var b = 30
//        print(b)
//    }
//}
//class classC: classB{
//    required init(){
//        var c = 40
//        print(c)
//    }
//}
//var objA = classA()
//var objB = classB()
//var objC = classC()

