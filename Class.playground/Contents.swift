//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
struct Grade {
    let letter: String
    let points: Double
    let credits: Double
}
class Student {
    var firstName: String
    var lastName: String
    var grades: [Grade] = []
    var credits = 0.0
    
    init(firstName: String, lastName: String) {
        self.firstName = firstName
        self.lastName = lastName
    }
    
    func recordGrade(_ grade: Grade) {
        grades.append(grade)
        credits += grade.credits
    }
}
//let jane = Student(firstName: "Jane", lastName: "Appleseed")
//let history = Grade(letter: "B", points: 9.0, credits: 3.0)
//var math = Grade(letter: "A", points: 16.0, credits: 4.0)
//jane.recordGrade(history)
//jane.recordGrade(math)
//math = Grade(letter: "A", points: 16.0, credits: 5.0)
//jane.recordGrade(math)
//print(jane.grades)
//jane.credits

//

class List{
    var name: String
    var set: String
    init(name: String,set: String) {
        self.name = name
        self.set = set
    }
    func printList(){
        print("\(name), set: \(set)")
    }
}
class User{
    var dicList: [String:String] = [:]
    var listFilm: List?
    func addList(list: List){
        listFilm = list
        dicList[list.name] = list.set
    }
    func  listForName(name: String) -> List?{
        return dicList.keys.contains(name) == true ? List(name: name, set: dicList[name]!) : nil
    }
}
var jane = User()
var john = User()

var actionFilm = List(name: "Star War", set: "Action")
jane.addList(list: actionFilm)
jane.listFilm?.set = "Action movie"

john.addList(list: actionFilm)
john.listFilm?.set = "Action horror movie"

print(john.dicList)
jane.addList(list: actionFilm)
print(jane.dicList)
actionFilm.printList()

//Ex2
struct Tshirt{
    var size: Int
    var color: String
    var piece: Double
    
}

class Users{
    var name: String?
    var email: String?
    var shoppingCart: ShoppingCart?
}

struct Address{
    var name: String
    var street: String
    var city: String
    var zipCode: Int
}

class ShoppingCart{
    var TShirt: [Tshirt]?
    var address: Address?
}
// i think class is call obj that exist only one 


