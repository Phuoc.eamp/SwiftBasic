//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
struct Circle {
    
    var radius: Double
    var area: Double {
        get{
            return .pi * radius * radius
        }
        set{
            radius = sqrt(newValue/(.pi))
        }
    }
    init (radius: Double) {
        self.radius = radius
    }
    
    mutating func grow(byFactor: Double) -> Double{
        area *= byFactor
        return area
    }
}
var a = Circle(radius: 3)
print(a.area)
a.grow(byFactor: 3)
print(a.area, a.radius)

//ex2

struct SimpleDate {
    var month: Int
    var day: Int
    var year: Int
    
    init(month: Int, day: Int, year: Int) {
        self.month = month
        self.day = day
        self.year = year
    }
    
    mutating func advance() {
        guard day < 32 && day > 0 else{
            print("Fill day!!!")
            return
        }
        switch month {
        case 1,3,5,7,8,10:
            month = day == 31 ? month + 1 : month
            day = day == 31 ? 1 : day + 1
            print(date)
        case 4,6,9,11:
            guard day < 31 else {
                print("Fill day!!!")
                return
            }
            month = day == 30 ? month + 1 : month
            day = day == 30 ? 1 : day + 1
            print(date)
        case 12:
            year = day == 31 ? year + 1 : year
            month = day == 31 ? 1 : 12
            day = day == 31 ? 1 : day + 1
            print(date)
        case 2:
            if (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0){
                month = day == 29 ? 3 : 2
                day = day == 29 ? 1 : day + 1
                print(date)
            }else {
                if day < 29 {
                    month = day == 28 ? 3 : 2
                    day = day == 28 ? 1 : 29
                    print(date)
                }else{
                    print("Fill day!!!")
                }
            }
        default:
            print("Fill Month!!!")
        }
    }
    
}

var date = SimpleDate(month: 12, day: 29, year: 2001)
date.advance()

//ex3 Add properties in Struct by using extensive
struct Math {
    static func factorial(of number: Int) -> Int {
        return (1...number).reduce(1, *)
    }
}
var math = Math()
extension Math {
    static func primeFactors(of number: Int) -> [Int] {
        var value = number
        var factor = 2
        var primes: [Int] = []
        while factor * factor <= value {
            if value % factor == 0 {
                primes.append(factor)
                value /= factor
            }
            else {
                factor += 1
            }
        }
        if value > 1 {
            primes.append(value)
        }
        return primes
    }
}
Math.primeFactors(of: 40)
