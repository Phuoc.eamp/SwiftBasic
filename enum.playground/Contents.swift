//: Playground - noun: a place where people can play

import UIKit

//class Student {
//    var name: String?
//    var age: Int?
//    init(name: String?, age: Int?) {
//        self.name = name
//        self.age = age
//    }
//}
//
//class BandMember: Student {
//    var minimumPracticeTime = 2
//}
//class OboePlayer: BandMember {
//
//    override var minimumPracticeTime: Int {
//        get {
//            return super.minimumPracticeTime * 2
//        }
//        set {
//            super.minimumPracticeTime = newValue / 2
//        }
//    }
//}
//var hallMonitor = Student(name: "john", age: 18)
//var oboeplayer = OboePlayer(name: "Johny", age: 18)
//if let hallMonitor = hallMonitor as? OboePlayer {
//    print("This hall monitor is a band member and practices at least \(hallMonitor.minimumPracticeTime) hours per week.")
//}
//class Person {
//    var firstName: String
//    var lastName: String
//    init(firstName: String, lastName: String) {
//        self.firstName = firstName
//        self.lastName = lastName
//    }
//}
//class Student: Person {
//    var grades: [Int] = []
//    func recordGrade(_ grade: Int) {
//        grades.append(grade)
//    }
//}
//class BandMember: Student {
//    var minimumPracticeTime = 2
//}
//class OboePlayer: BandMember {
//
//    override var minimumPracticeTime: Int {
//        get {
//            return super.minimumPracticeTime * 2
//        }
//        set {
//            super.minimumPracticeTime = newValue / 2
//        }
//    }
//}
//let person = Person(firstName: "Johnny", lastName: "Appleseed")
//let oboePlayer = OboePlayer(firstName: "Jane", lastName: "Appleseed")
//var hallMonitor = Student(firstName: "Jill", lastName: "Bananapeel")
//(hallMonitor as? BandMember)?.minimumPracticeTime

class Media {
    var name: String
    init(name: String) {
        self.name = name
    }
}
class Movie: Media{
    var direction: String
    init(name: String, direction: String) {
        self.direction = direction
        super.init(name: name)
    }
}
class Song: Media {
    var singer: String
    init(name: String, singer: String) {
        self.singer = singer
        super.init(name: name)
    }
}

let library = [
    Movie(name: "Star War", direction: "ABC"),
    Song(name: "aaaaa", singer: "bbbbb"),
    Song(name: "ccccc", singer: "ccccc"),
    Movie(name: "X-men", direction: "20th"),
    Movie(name: "Avergers", direction: "Marvel"),
    Media(name: "Sh"),
    nil
]
//print("There are \(library.filter{$0 is Movie}.count) movies and \(library.filter{$0 is Song}.count)")
var movieCount = 0
var songCount = 0
library.filter {
    if $0 is Movie {
        movieCount += 1
        return true
    }else {
        songCount += 1
        return false
    }
}
print("There are \(movieCount) movies and \(songCount)")
library.forEach{
    if let movie = $0 as? Movie {
        print("Movie: \(movie.name), dir. \(movie.direction)")
    }else if let song = $0 as? Song {
        print("Song: \(song.name), by \(song.singer)")
    }
}
print("-------------------")
var thing : [Any] = [
    0,
    (3,6),
    "John",
    Movie(name: "Star", direction: "aaa"),
    { (name: String) -> String in "Hello \(name)" },
    "Army",
    0.0
]
thing.map{
    switch $0 {
    case let number as Int:
        print("Number \(number)")
        break
    case let name as String:
        print("Name: \(name)")
        break
    case let movie as Movie:
        print("Movie: \(movie.name), dir. \(movie.direction)")
        break
    case let item as (String) -> String:
        print(item("Alex"))
        break
    default:
        break
    }
}
print("------------")
enum Coin: Int {
    case penny = 1
    case nickel = 5
    case dime = 10
    case quarter = 25
}
let coinPurse: [Coin] = [.penny, .quarter, .nickel, .dime, .penny, .dime,
                         .quarter]
print(coinPurse.map{$0.rawValue})
print(coinPurse.filter {$0 == Coin.penny}.count)
var dict = [Int: Int]()
coinPurse.map { (coin: Coin) in
    dict[coin.rawValue] = coinPurse.filter {$0 == coin}.count
}

print(dict)
print("-----------------")
enum Month: Int {
    case january = 1, february, march, april, may, june, july,
    august, september, october, november, december
    var untilSummer : Int {
        return Month.december.rawValue - rawValue
    }
}
var months = Month(rawValue: 7)

print(months!.untilSummer)
print("----------------")
enum Direction {
    case north
    case south
    case east
    case west
    
}

var movements: [Direction] = []
movements.append(.north)
movements.append(.south)
movements.append(.east)
var location = (x: 0 , y: 0)
movements.forEach {
    guard location.x >= 0 && location.y <= 0 else {
        print("Can't move!!!")
        return
    }
    switch $0 {
    case Direction.north:
        location.x += 1
        break
    case Direction.south:
        if location.x >= 1{
            location.x -= 1
        }else {
            print("Can't move!!!")
        }
        break
    case Direction.east:
        location.y += 1
        break
    case Direction.west:
        if location.y >= 1{
            location.y -= 1
        }else {
            print("Can't move!!!")
        }
        break
    default:
        break
    }
}
print(location)
print("--------------")
extension Direction {
    var move: (Int, Int) {
        var moved = (0,0)

        switch self {
        case Direction.north:
            moved.0 += 1
            break
        case Direction.south:
            if moved.0 >= 1{
                moved.0 += 1
            }else {
                print("Can't move!!!")
            }
            break
        case Direction.east:
            moved.1 += 1
            break
        case Direction.west:
            if moved.0 >= 1{
                moved.0 += 1
            }else {
                print("Can't move!!!")
            }
            break
        default:
            break
        }
        return moved
    }
    func moved() {
        
    }
    
}
var direc = Direction.north
print(direc.move)
direc = .east
print(direc.move)
print("------------------")
print("Default enum")


