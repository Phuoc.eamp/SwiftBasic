//: Playground - noun: a place where people can play

import UIKit
import Foundation
//protocol Comparable {
//    static func < (lhs: Self, rhs: Self) -> Bool
//    static func == (lhs: Self, rhs: Self) -> Bool
//    static func > (lhs: Self, rhs: Self) -> Bool
//}
//
//struct Date {
//    let year: Int
//    let month: Int
//    let day: Int
//}
//extension Date: Comparable {
//    static func == (lhs: Date, rhs: Date) -> Bool {
//        return lhs.year == rhs.year && lhs.month == rhs.month && lhs.day == rhs.day
//    }
//    static func > (lhs: Date, rhs: Date) -> Bool {
//        if lhs.year != rhs.year {
//            return lhs.year > rhs.year
//        } else if lhs.month != rhs.month {
//            return lhs.month > rhs.month
//        } else {
//            return lhs.day > rhs.day
//        }
//    }
//
//    static func < (lhs: Date, rhs: Date) -> Bool {
//        if lhs.year != rhs.year {
//            return lhs.year < rhs.year
//        } else if lhs.month != rhs.month {
//            return lhs.month < rhs.month
//        } else {
//            return lhs.day < rhs.day
//        }
//    }
//}
//
//let day1 = Date(year: 2009, month: 9, day: 27)
//let day2 = Date(year: 2009, month: 9, day: 28)
//day1 < day2
//day1 > day2
//day1 == day2
//
protocol Work: class {
    func work(job: String)
}

class Boss {
    var work: Work?
    deinit {
        print("CAN'T")
    }
}

class worker: Work {
    func work(job: String) {
        print(job)
    }
    deinit {
        print("CAN WORK")
    }
}

var John = Boss()
var me = worker()
John.work = me
John.work?.work(job: "do something")

// why retain cycle
//
//protocol Init {
//
//    init()
//    init(a: String, b: String)
//}
//
//class A: Init {
//    var a: String
//    var b: String
//
//    required init() {
//        self.a = "Hello"
//        self.b = "John"
//        print("\(a) \(b)!")
//    }
//
//    required init(a: String, b: String) {
//        self.a = a
//        self.b = b
//    }
//}

//protocol Equatable {
//    static func ==(lhs: Self, rhs: Self) -> Bool
//}

protocol comparable: Equatable {
    static func <(lhs: Self, rhs: Self) -> Bool
    static func <=(lhs: Self, rhs: Self) -> Bool
    static func >=(lhs: Self, rhs: Self) -> Bool
    static func >(lhs: Self, rhs: Self) -> Bool
}

protocol Hashable : Equatable {
    var hashValue: Int { get }
}

class Student {
    let email: String
    var firstName: String
    var lastName: String
    init(email: String, firstName: String, lastName: String) {
        self.email = email
        self.firstName = firstName
        self.lastName = lastName
    }
}

extension Student: Equatable {
    static func ==(lhs: Student, rhs: Student) -> Bool {
        return lhs.email == rhs.email
    }
}
extension Student: Hashable {
    var hashValue: Int {
        return email.hashValue
    }
}



extension Student: CustomStringConvertible {
    var description: String {
        return "\(firstName) \(lastName)"
    }
}


let johnn = Student(email: "johnny.appleseed@apple.com", firstName:
    "Johnny", lastName: "Appleseed")
var copy = johnn
copy.firstName = "John"
let alex = Student(email: "alex@gmail.com", firstName: "Alex", lastName: "Swift")
print(johnn.hashValue)
print(alex.hashValue)

print(johnn)

protocol Vehicle {
    func accelerate()
    func stopping()
}

protocol WheeledVehicle: Vehicle {
    var numberOfWheels: Int { get }
    var wheelSize: Double { get set }
}

class Bike: Vehicle, WheeledVehicle {
    let numberOfWheels: Int = 2
    var wheelSize: Double
    var run = true
    var stop = true
    var name: String
    init(name: String, wheelSize: Double) {
        self.name = name
        self.wheelSize = wheelSize
    }
    func accelerate() {
        run = true
        stop = false
    }
    
    func stopping() {
        run = false
        stop = true
    }
    
}
extension Bike: CustomStringConvertible, Equatable, Comparable {
    static func < (lhs: Bike, rhs: Bike) -> Bool {
        return lhs.wheelSize < rhs.wheelSize
    }
   
    
    static func == (lhs: Bike, rhs: Bike) -> Bool {
        return lhs.wheelSize == rhs.wheelSize
    }
    
    
    var description: String {
        return "\(name) has wheel Size: \(wheelSize)"
    }
}


var blueBike = Bike(name: "Blue Bike", wheelSize: 4.0)
var yellowBike = Bike(name: "Yellow bike", wheelSize: 9.0)
var redBike = Bike(name: "Red Bike", wheelSize: 16.0)
var ironBike = Bike(name: "Iron Bike", wheelSize: 5.0)
var inoxBike = Bike(name: "inow Bike", wheelSize: 17.0)

var bikes : [Bike] = [blueBike, yellowBike, redBike, ironBike, inoxBike]
print(bikes)
print(bikes.sorted())
//print(bikes.max())
bikes.contains(blueBike)
bikes.starts(with: [blueBike, yellowBike])
//bikes.sorted()
var arrr = bikes.sorted { (a: Bike, b: Bike) -> Bool in
    a.wheelSize > b.wheelSize
}
print(arrr)
var arr = [1,3,2,3,4,5,6,6,7,8]
arr.sorted()

struct Record {
    var wins: Int
    var losses: Int
}

extension Record: Equatable {
    static func ==(lhs: Record, rhs: Record) -> Bool {
        return lhs.wins == rhs.wins &&
            lhs.losses == rhs.losses
    }
}

let teamA = Record(wins: 14, losses: 11)
let teamB = Record(wins: 23, losses: 8)
let teamC = Record(wins: 23, losses: 9)
var leagueRecords = [teamA, teamB, teamC]
print(leagueRecords)

protocol TrafficLight {
    func stop() -> String
    func run() -> String
    func prepare() -> String
}

enum TrafficColors: Int, TrafficLight {
    func stop() -> String {
        return "Stop now"
    }
    
    func run() -> String {
        return "Can go"
    }
    
    func prepare() -> String {
        return "Prepare"
    }
    
    case red = 1
    case yellow
    case blue
    
    var color: String {
        switch self {
        case .blue:
            return run()
        case .yellow:
            return prepare()
        case .red:
            return stop()
        }
    }
}

var traffic = TrafficColors(rawValue: 2)
traffic?.color

protocol Dosomething {
    associatedtype job
   
}
struct Job: Dosomething {
    typealias job = String
    
}

protocol Type {}

var type = [Type]()
//
protocol CanMakeSound {
    func sound()
}
protocol CanEat {
    mutating func eat()
}


struct Dog: CanMakeSound {
    
    func sound() {
        print("gau gau")
    }
}


class Cat: CanMakeSound {
    func sound() {
        print("meo meo")
    }
}


enum DogStatus: CanMakeSound {
    
    case Normal, BeHungry
    
    func sound() {
        switch self {
        case .Normal:
            print("gau gau")
        case .BeHungry:
            print("gu gu ...")
        }
        
    }
}

extension DogStatus: CanEat {
    mutating func eat() {
        switch self {
        case .Normal:
            print("I am not hungry ")
            
        case .BeHungry:
            print("I am eating ... ")
            self = .Normal
        }
    }
}

let lulu = Dog()
lulu.sound()
var kiki = DogStatus.Normal

@objc protocol pet {
    func fed()
    @objc optional func clean()
    func sleep()
}

class Pig: pet {
    func sleep() {
        <#code#>
    }
    
    func fed() {
        <#code#>
    }
    

}
//
protocol A {
    //associatedtype T
    var rawValue: String {get}
}

extension A {
    var key: String {
        return rawValue
    }
    var hashValue : Int {
        return key.hashValue
    }
}


enum B: String, A {
    case a = "abc"
    case b = "bac"
    case c = "cab"
    var value: Int {
        return self.hashValue
    }
}

var b = B.c
b.key
b.value
print(b.key)

//public protocol UD {
//    associatedtype T
//    var rawValue: String {get}
//}
//
//enum


