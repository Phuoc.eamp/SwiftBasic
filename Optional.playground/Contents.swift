//: Playground - noun: a place where people can play

import UIKit



// define optional
var name: String? = "Ray"
var age: Int? = nil
let distance: Float = 26.7
var middleName: String? = nil

//Divide and conquer
func devideIfWhole(value: Int, by divisor: Int) -> Int?{
    if value % divisor == 0 {
        return value / divisor
    }else{
        return nil
    }
}
func Result(value: Int, divisor: Int){
    guard let answer = devideIfWhole(value: value, by: divisor) else{
        print("Not divisible 🙂.")
        return
    }
    print("Yep, it divides \(answer) times")
}
Result(value: 10, divisor: 3)

//nil coalescing
func refactor(value: Int, divisor: Int){
    let result : Int? = devideIfWhole(value: value, by: divisor)
    let answer = result ?? 0
    print("It divides \(answer) times.")
}
refactor(value: 10, divisor: 3)

//nested optional
let number: Int??? = 10
//
print(number!!!)
if number != nil{
    print(number!!!)
}
//
if let a = number!!{
    print(a)
}
//
func nestedOptional(number: Int???){
    guard let a = number!! else{
        return
    }
    print(a)
}
nestedOptional(number: number)

