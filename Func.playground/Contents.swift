//: Playground - noun: a place where people can play

import UIKit

// check number divisor
func isNumberDivisible(number: Int, divisor: Int) -> Bool {
    if number % divisor == 0 {
        return true
    }else {return false}
}
isNumberDivisible(number: 0, divisor: 5)

// check prime
func isPrime(number: Int) -> Bool {
    if number == 1 || number == 2{
        print(number)
        return true
    }else {
        for i in 2..<number{
            if number % i == 0 {
                return false
            }
        }
        print(number)
        return true
    }
}
isPrime(number: 28)

// print prime in range
//func Prime(from: Int, to: Int){
//    for i in from...to {
//        isPrime(number: i)
//    }
//}
//Prime(from: 0, to: 9)
//this is error

//Fibonacci
func fibonacci(number: Int) -> Int{
    var f0 = 0
    var f1 = 1
    var fn = 1
    if number < 0 {
        return 0
    }else if number == 1 || number == 2 {
        return 1
    }else {
        for _ in 3...number{
            f0 = f1
            f1 = fn
            fn = f0 + f1
        }
    }
    return fn
}
fibonacci(number: 1)
fibonacci(number: 2)
fibonacci(number: 3)
fibonacci(number: 4)
fibonacci(number: 10)
